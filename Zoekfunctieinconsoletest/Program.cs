﻿using System;
using System.Data.SQLite;

namespace Zoekfunctieinconsoletest
{
    class Program
    {
        static void Main(string[] args)
        {
            string tagIdZoeker = "SELECT tags.id FROM tags WHERE tags.name = @parameter"; // Vraagt de id van tag ... op, voor gebruik in de volgende query
            string SQLsearch = "SELECT files.path FROM entries JOIN files ON entries.file_id = files.id WHERE entries.id IN (SELECT tagmap.entry_id FROM tagmap WHERE tagmap.tag_id = @parameter)"; // Vraagt de bestanden uit de database met de tag met id ... op
            using (SQLiteConnection connect = new SQLiteConnection(@"Data Source = testdatabase.sqlite3; Version = 3"))
            using(SQLiteCommand tagIdCommand = new SQLiteCommand(tagIdZoeker, connect))
            using (SQLiteCommand command = new SQLiteCommand(SQLsearch, connect))
            {
                connect.Open();

                Console.WriteLine("Tag om op te zoeken?");
                string tag = Console.ReadLine();
                tagIdCommand.Parameters.Add(new SQLiteParameter("@parameter", tag));
                SQLiteDataReader tagreader = tagIdCommand.ExecuteReader();
                string tagid = ""; 
                while(tagreader.Read())
                {
                    tagid = tagreader["id"].ToString();
                }

                command.Parameters.Add(new SQLiteParameter("@parameter", tagid));
                SQLiteDataReader reader = command.ExecuteReader();
                {
                    while (reader.Read())
                    {
                        Console.WriteLine(reader["path"]);
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
